# SY32 Projet P24



## Sujet

Ce projet est réalisé dans le cadre de l'UV SY32 au semestre P24. Il vise à implémenter des méthodes de détection et de classification de panneaux de signalisation routière.

## Rapport 

Un rapport de projet déposé sur Moodle détail les méthodes utilisées pour ce projet.  

## Détection par méthodes classique

Le code utilisé pour la détection de panneaux de signalisation routière par méthodes classique se situe dans le répertoire suivant : 

__Methode_Classique__


## Détection par méthodes d'apprentissage profond

Le code utilisé pour la détection de panneaux de signalisation routière par méthodes d'apprenstissage profond se situe dans le répertoire suivant : 

__Apprentissage_Profond__

