import csv
import os

import pandas as pd

from generation_multilabel_dataset import is_csv_first_line_empty


def compile_csv(csv_path, output_path = "Results/detection.csv"):
    with open(output_path, mode='w', newline='') as file:
        writer = csv.writer(file)

        # Parcourir tous les fichiers du dossier
        for filename in os.listdir(csv_path):

            # Construire le chemin complet du fichier
            filepath = os.path.join(csv_path, filename)

            # Vérifier si c'est un fichier (et non un dossier)
            if os.path.isfile(filepath):
                filename_csv = os.path.join(csv_path, filename)

                img_number = filename.split('/')[-1].split('.')[0]  # Extract image number from filename
                print(img_number)
                # Vérifier que le fichier de labels n'est pas vide
                if not is_csv_first_line_empty(filename_csv):
                    # lecture des labels
                    labels = pd.read_csv(filename_csv, header=None)

                    # parcours de chaque label
                    for index, row in labels.iterrows():
                        rowList = row.to_list()
                        rowList.insert(0, int(img_number))
                        rowList[-1],rowList[-2] = rowList[-2],rowList[-1]
                        print(rowList)
                        writer.writerow(rowList)

compile_csv("detection_label/")