#%%
import os
import pandas as pd
from generation_multilabel_dataset import multilabel_dataset, spetialized_dataset
from sklearn.svm import SVC
import joblib

def create_classifier(X_train, y_train, clf_name):
    '''
    Creer un classifieur pour un type de panneaux
    '''

    svm_clf = SVC(kernel='linear', probability=True)
    svm_clf.fit(X_train, y_train)
    joblib.dump(svm_clf, clf_name)

def test_classifieur(X_val, y_val, clf_name):
    '''
    Test le classfieur sur les données val
    '''

    svm_clf = joblib.load(clf_name)
    score = svm_clf.score(X_val, y_val)
    print('Score : ', score)


# nom des dossiers pour train
img_train = '../dataset/train/images/'
label_train = '../dataset/train/labels/'

# nom des dossiers pour val
img_val = '../dataset/val/images/'
label_val = '../dataset/val/labels/'

'''# train
X_train, y_train = multilabel_dataset(img_train, label_train)

clf_name = f'classifiers/svm_clf_sign.pkl'

create_classifier(X_train, y_train, clf_name)

# Val

X_val, y_val = multilabel_dataset(img_val, label_val)


test_classifieur(X_val, y_val, clf_name)'''

X_type_train, Y_type_train, X_sign_train, Y_sign_train, X_light_train, Y_light_train = spetialized_dataset(img_train, label_train)
X_type_val, Y_type_val, X_sign_val, Y_sign_val, X_light_val, Y_light_val = spetialized_dataset(img_val, label_val)
clf_type_name = f'classifiers/spe/svm_clf_type.pkl'
clf_sign_name = f'classifiers/spe/svm_clf_sign.pkl'
clf_light_name = f'classifiers/spe/svm_clf_light.pkl'

print("training type classifier ")
create_classifier(X_type_train, Y_type_train, clf_type_name)
print("testing type classifier ")
test_classifieur(X_type_val, Y_type_val, clf_type_name)

print("training sign classifier ")
create_classifier(X_sign_train, Y_sign_train, clf_sign_name)
print("testing sign classifier ")
test_classifieur(X_sign_val, Y_sign_val, clf_sign_name)

print("training light classifier ")
create_classifier(X_light_train, Y_light_train, clf_light_name)
print("testing light classifier ")
test_classifieur(X_light_val, Y_light_val, clf_light_name)








