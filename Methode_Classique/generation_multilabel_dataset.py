# %%

import matplotlib.pyplot as plt
import os
from PIL import Image, ImageDraw, ImageFont, ImageColor
import pandas as pd
import csv

from shapely import Polygon
from skimage import io
import random
import numpy as np
from skimage import transform
from skimage import img_as_ubyte

from detection import detection_image


# Fonction pour vérifier si la première ligne d'un fichier CSV est vide
def is_csv_first_line_empty(file_path):
    '''
    Verifie si un fichier .csv de label est vide 
    Renvoie vrai si le fichier est vide
    '''

    with open(file_path, 'r') as file:
        reader = csv.reader(file)
        first_line = next(reader, None)  # Lire la première ligne
        if first_line is None:  # Vérifier si le fichier est complètement vide
            return True
        # Vérifier si tous les champs de la première ligne sont vides
        return all(field.strip() == '' for field in first_line)


# fonction qui affiche
def plot_traffic_sign_img(img, name):
    fig, ax = plt.subplots()
    ax.imshow(img)
    ax.set_title(name)
    ax.axis('off')
    plt.show()


# fonction qui génère une
def generate_random_image(img, labels):
    '''
    genere une image aléatoire dans img de la même taille que img_traffic_sign
    '''

    # Génération d'une partie aléatoire de l'image de même taille
    height, width = labels[3][0] - labels[1][0], labels[2][0] - labels[0][0]
    img_height, img_width, _ = img.shape

    # Assurez-vous que la partie aléatoire se trouve dans les limites de l'image
    max_x = img_width - width
    max_y = img_height - height
    polys = []
    for index, row in labels.iterrows():
        # img[row[1]:row[3], row[0]:row[2]]
        polys.append(Polygon(
            [(row[0], row[1]), (row[2], row[1]), (row[2], row[3]),
             (row[0], row[3])])
        )
    try_nb = 0
    while try_nb < 100:
        try_nb += 1
        random_x = random.randint(0, max_x)
        random_y = random.randint(0, max_y)
        if random_x > width and random_y > height:
            continue
        randomPoly = Polygon(
            [(random_x, random_y), (random_x + img_width, random_y), (random_x + img_width, random_y + img_height),
             (random_x, random_y + img_height)])
        inter = False
        for poly in polys:
            if poly.intersects(randomPoly):
                inter = True
                continue
        if inter:
            continue
        img_random = img[random_y:random_y + height, random_x:random_x + width]

        return img_random
    return None


def multilabel_dataset(img_folder, label_folder, nb_images=None,
                       plot_traffic_sign=False, noRandom=False):
    '''Renvoie les images et labels d'un jeu de données
    Genère aléatoirement '''

    labels_list = ['empty',
                   'danger',
                   'interdiction',
                   'obligation',
                   'stop',
                   'ceder',
                   'frouge',
                   'forange',
                   'fvert', ]

    if not nb_images:
        # recuperation des fichiers des images
        images_file = [os.path.join(img_folder, l)
                       for l in os.listdir(img_folder)]

        # recuperation des fichiers des labels
        labels_file = [os.path.join(label_folder, l)
                       for l in os.listdir(label_folder)]
    else:
        # recuperation des fichiers des images
        images_file = [os.path.join(img_folder, l)
                       for l in os.listdir(img_folder)[:nb_images]]

        # recuperation des fichiers des labels
        labels_file = [os.path.join(label_folder, l)
                       for l in os.listdir(label_folder)[:nb_images]]

    # initialisation des listes qui vont contenir les images
    images_traffic_sign = []
    labels_traffic_sign = []

    for i, label_file in enumerate(labels_file):

        try:
            # Vérifier que le fichier de labels n'est pas vide
            if not is_csv_first_line_empty(label_file):

                # lecture des labels
                labels = pd.read_csv(label_file, header=None)

                # lecture de l'image
                # img = Image.open(images_file[i])
                img = io.imread(images_file[i])

                # parcours de chaque label
                for index, row in labels.iterrows():

                    # lecture du label du panneau
                    traffic_sign_name = row[4]

                    img_traffic_sign = img[row[1]:row[3], row[0]:row[2]]
                    # reshape de l'image
                    img_traffic_sign = transform.resize(
                        img_traffic_sign, (48, 48),
                        anti_aliasing=True, mode='reflect')

                    # Convertir les valeurs des pixels à l'échelle [0, 255]
                    img_traffic_sign = img_as_ubyte(img_traffic_sign)
                    img_traffic_sign = np.clip(img_traffic_sign, 0, 255)

                    # ajout du panneau en flatten et du label
                    images_traffic_sign.append(img_traffic_sign.flatten())
                    labels_traffic_sign.append(traffic_sign_name)

                    # pour chaque image avec un panneau, on rajoute 2 images
                    # sans panneau
                    if not noRandom:
                        for _ in range(3):
                            try:
                                img_random = generate_random_image(
                                    img, labels)

                                # reshape de l'image
                                img_random = transform.resize(
                                    img_random, (48, 48),
                                    anti_aliasing=True, mode='reflect')
                                img_random = img_as_ubyte(img_random)
                                # Convertir les valeurs des pixels à l'échelle [0, 255]
                                img_random = np.clip(img_random, 0, 255)
                                images_traffic_sign.append(
                                    img_random.flatten())
                                labels_traffic_sign.append('empty')

                                if plot_traffic_sign:
                                    plot_traffic_sign_img(img_random, 'Random')

                            except:
                                continue
                    # affichage du panneau et du nom
                    if plot_traffic_sign:
                        plot_traffic_sign_img(
                            img_traffic_sign, traffic_sign_name)

        except:
            print('Erreur sur lecture du fichier label.csv ; image suivant')

    return images_traffic_sign, labels_traffic_sign


def spetialized_dataset(img_folder, label_folder, nb_images=None,
                        plot_traffic_sign=False, noRandom=False):
    '''Renvoie les images et labels d'un jeu de données
    Genère aléatoirement '''

    labels_list = ['empty',
                   'danger',
                   'interdiction',
                   'obligation',
                   'stop',
                   'ceder',
                   'frouge',
                   'forange',
                   'fvert', ]

    if not nb_images:
        # recuperation des fichiers des images
        images_file = [os.path.join(img_folder, l)
                       for l in os.listdir(img_folder)]

        # recuperation des fichiers des labels
        labels_file = [os.path.join(label_folder, l)
                       for l in os.listdir(label_folder)]
    else:
        # recuperation des fichiers des images
        images_file = [os.path.join(img_folder, l)
                       for l in os.listdir(img_folder)[:nb_images]]

        # recuperation des fichiers des labels
        labels_file = [os.path.join(label_folder, l)
                       for l in os.listdir(label_folder)[:nb_images]]

    # initialisation des listes qui vont contenir les images
    images_sign_type = []
    labels_sign_type = []
    images_sign = []
    labels_sign = []
    images_light = []
    labels_light = []
    for i, label_file in enumerate(labels_file):

        try:
            # Vérifier que le fichier de labels n'est pas vide
            if not is_csv_first_line_empty(label_file):

                # lecture des labels
                labels = pd.read_csv(label_file, header=None)

                # lecture de l'image
                # img = Image.open(images_file[i])
                img = io.imread(images_file[i])

                # parcours de chaque label
                for index, row in labels.iterrows():

                    # lecture du label du panneau
                    traffic_sign_name = row[4]
                    if row[3] - row[1] > row[2] - row[0]:
                        img_traffic_sign = img[row[1]:row[3], row[0]:row[0] + row[3] - row[1]]
                    else:
                        img_traffic_sign = img[row[1]:row[1] + row[2] - row[0], row[0]:row[2]]

                    # reshape de l'image
                    img_traffic_sign = transform.resize(
                        img_traffic_sign, (48, 48),
                        anti_aliasing=True, mode='reflect')

                    # Convertir les valeurs des pixels à l'échelle [0, 255]
                    img_traffic_sign = img_as_ubyte(img_traffic_sign)
                    img_traffic_sign = np.clip(img_traffic_sign, 0, 255)

                    if traffic_sign_name in ['danger', 'interdiction', 'obligation', 'stop', 'ceder']:
                        # ajout du panneau en flatten et du label
                        images_sign_type.append(img_traffic_sign.flatten())
                        labels_sign_type.append("sign")
                        images_sign.append(img_traffic_sign.flatten())
                        labels_sign.append(traffic_sign_name)
                    elif traffic_sign_name in ['frouge', 'forange', 'fvert']:
                        images_sign_type.append(img_traffic_sign.flatten())
                        labels_sign_type.append("light")
                        images_light.append(img_traffic_sign.flatten())
                        labels_light.append(traffic_sign_name)
                    else:
                        images_sign_type.append(img_traffic_sign.flatten())
                        labels_sign_type.append("empty")

                    # pour chaque image avec un panneau, on rajoute 2 images
                    # sans panneau
                    if not noRandom:
                        for _ in range(3):
                            try:
                                img_random = generate_random_image(
                                    img, labels)

                                # reshape de l'image
                                img_random = transform.resize(
                                    img_random, (48, 48),
                                    anti_aliasing=True, mode='reflect')
                                img_random = img_as_ubyte(img_random)
                                # Convertir les valeurs des pixels à l'échelle [0, 255]
                                img_random = np.clip(img_random, 0, 255)
                                images_sign_type.append(
                                    img_random.flatten())
                                images_sign_type.append('empty')

                                if plot_traffic_sign:
                                    plot_traffic_sign_img(img_random, 'Random')

                            except:
                                continue
                    # affichage du panneau et du nom
                    if plot_traffic_sign:
                        plot_traffic_sign_img(
                            img_traffic_sign, traffic_sign_name)

        except:
            print('Erreur sur lecture du fichier label.csv ; image suivant')

    return images_sign_type, labels_sign_type, images_sign, labels_sign, images_light, labels_light


from multiprocessing import Pool


def detect_wrong(images_file, correct_labels, overlap_threshold = 0.2, threshold = 0.9, show = True):
    if os.path.isfile(images_file):
        res = detection_image(images_file, None, None,
                              None, display_fig=False)
    else:
        return

    false_positives = []
    labels_rectangles = [
        (row[4],
         Polygon([(row[0], row[1]), (row[2], row[1]), (row[2], row[3]), (row[0], row[3])]))
        for index, row in correct_labels.iterrows()]

    # liste des noms des labels
    # labels_list = clf.classes_
    img = io.imread(images_file)
    for label in res:
        pass
        rect = Polygon([(label[0], label[1]), (label[2], label[1]), (label[2], label[3]), (label[0], label[3])])
        found = False
        best_recouvrement = None
        best_recouvrement_label = 'empty'
        for c_label, c_label_rect in labels_rectangles:
            intersection = c_label_rect.intersection(rect)
            union = c_label_rect.union(rect)
            recouvrement = intersection.area / union.area
            if recouvrement >= overlap_threshold or (
                    intersection.area > c_label_rect.area * 0.7 and recouvrement >= 0.1):
                if best_recouvrement is None:
                    best_recouvrement = recouvrement
                    best_recouvrement_label = c_label
                elif recouvrement > best_recouvrement:
                    best_recouvrement = recouvrement
                    best_recouvrement_label = c_label
        if label[4] != best_recouvrement_label and label[5] >= threshold:
            detected_img = img[label[1]:label[3], label[0]:label[0] + label[3] - label[1]]
            false_positives.append((best_recouvrement_label, detected_img))
            if show:
                fig3, ax3 = plt.subplots()
                ax3.imshow(detected_img)
                ax3.axis('off')
                ax3.set_title(str("false negatives, detected: {} instead of : {}".format(label[4],
                                                                                         best_recouvrement_label)))
                plt.show()

    return false_positives


treated_image_nb = 0


def analyse(data):
    images_file, label_file, save_path = data
    # Vérifier que le fichier de labels n'est pas vide
    if not is_csv_first_line_empty(label_file):
        img_number = images_file[-8:-4]
        errors = detect_wrong(images_file, pd.read_csv(label_file, header=None))
        save_num = 0
        if len(errors) > 0:
            print("false positive detected")
        for y, (label, img) in enumerate(errors):
            img_reshape = img_as_ubyte(img)
            img_reshape = np.clip(img_reshape, 0, 255)
            label_filename = 'labels/' + '{0:04d}'.format(save_num) + '.csv'
            image_filename = 'images/' + '{0:04d}'.format(save_num) + '.png'
            while os.path.exists(os.path.join(save_path, image_filename)) or os.path.exists(
                    os.path.join(save_path, label_filename)):
                save_num += 1
                label_filename = 'labels/' + '{0:04d}'.format(save_num) + '.csv'
                image_filename = 'images/' + '{0:04d}'.format(save_num) + '.png'
            io.imsave(os.path.join(save_path, image_filename), img_reshape)
            with open(os.path.join(save_path, label_filename), 'w') as f:
                f.write("{},{},{},{},{}".format(0, 0, img.shape[1], img.shape[0], label))
        global treated_image_nb
        treated_image_nb += 1
        print("image treated " + str(treated_image_nb))


def FindFalsePositive(img_folder, label_folder, save_path="train_false_pos", multiproc=False):
    # recuperation des fichiers des images
    images_file = [os.path.join(img_folder, l)
                   for l in os.listdir(img_folder)]

    # recuperation des fichiers des labels
    labels_file = [os.path.join(label_folder, l)
                   for l in os.listdir(label_folder)]
    total_images = 0

    if multiproc:
        global treated_image_nb
        treated_image_nb = 0
        poolWork = [(images_file[i], l, save_path) for i, l in enumerate(labels_file)]
        p = Pool(9)
        p.map(analyse, poolWork)
    else:
        for i, label_file in enumerate(labels_file):
            analyse((images_file[i], label_file, save_path))

if __name__ == "__main__":
    FindFalsePositive("../dataset/val/images/", "../dataset/val/labels/")