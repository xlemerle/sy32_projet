# Generation d'un classifieur pour la classification

Les fichiers ```generation_multilabel_dataset.py``` et ```clf_mutlilabel.py``` permettent de générer le classifieur SVM pour la classification des panneaux de signalisation. 

Les classifieurs se situents dans le repertoire ```classifiers```. 

# Detection des panneaux

Le fichier ```detection.py``` permet de détecter les panneaux de signalisation sur les images. Le fichier ```non_maxima.py``` supprime les non-maxima. 

Les images et labels avec les panneaux détecté se situent respectivement dans les fichier ```detection_img``` et ```detection_label```.

# Calcul du score

Le fichier ```score.py``` calcul le score pour les images de validation. Le fichier ```compile_csv.py``` compile tous les résulat dans un seul fichier .csv, disponible dans le fichier ```Results/detection.csv```. 
