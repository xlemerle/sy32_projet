# %%
import matplotlib.pyplot as plt
import os
import csv
from skimage import io
import numpy as np
from skimage import transform
from skimage import img_as_ubyte
import joblib
from non_maxima import delete_non_maxima_image


def recursive_sliding_windows(img_resized, window_size, step_size, spetialized_classifier=False, display_fig=False):
    global clf_type, clf_sign, clf_light, clf
    labels_list_detection_sliding_windows = []

    for y in range(0, img_resized.shape[0] - window_size + 1, step_size):
        for i, x in enumerate(range(0, img_resized.shape[1] - window_size + 1, step_size)):

            window = img_resized[y:y + window_size, x:x + window_size]
            # reshape de l'image
            window_reshape = transform.resize(
                window, (48, 48),
                anti_aliasing=True, mode='reflect')

            # Convertir les valeurs des pixels à l'échelle [0, 255]
            window = img_as_ubyte(window_reshape)
            window = np.clip(window, 0, 255)

            # ajout du panneau en flatten et du label
            window_flatten = window.flatten().reshape(1, -1)

            # test du clf
            if spetialized_classifier:
                result_clf = clf_type.predict_proba(window_flatten)[0]
            else:
                result_clf = clf.predict_proba(window_flatten)[0]

            if display_fig:
                # create a figure to display the image with the current window highlighted
                fig, ax = plt.subplots()
                ax.imshow(img_resized)
                ax.axis('off')

                # draw rectangle around the current window on the image
                rect = plt.Rectangle((x, y), window_size, window_size, edgecolor='red', facecolor='none')
                ax.add_patch(rect)

                # display the figure with the rectangle
                plt.show()
            labels_name = clf_type.classes_ if spetialized_classifier else clf.classes_
            labels_name_sign = clf_sign.classes_
            labels_name_light = clf_light.classes_
            for i, result_clf_label in enumerate(result_clf):
                if labels_name[i] == 'ff' or labels_name[i] == 'empty':
                    continue
                if result_clf_label > 0.9:
                    final_label = labels_name[i]
                    labels_list_detection_sliding_windows.append(
                        [x, y, window_size, window_size, final_label, result_clf_label])

    return labels_list_detection_sliding_windows


def save_list_csv(list, filename):
    # Ouverture du fichier en mode écriture
    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file)

        # Écriture des données dans le fichier CSV
        for row in list:
            writer.writerow(row)


def detection_image(img_filename, label_filename, img_to_save_filename, spetialized_classifier=False,
                    display_fig=False):
    '''detection des panneaux pour 1 image'''

    img = io.imread(img_filename)
    img_number = img_filename[-8:-4]
    # print(img_number)

    # resize the image
    # img_resized = transform.resize(img, (1000, 1000), anti_aliasing=True, mode='reflect')
    img_resized = img
    # create a figure to display the sliding windows
    if display_fig:
        fig, ax = plt.subplots()
        ax.imshow(img_resized)
        ax.axis('off')
        plt.show()

    labels_list_detection = []

    # recouvrement variable
    list_window_size_step = [[1, 1],
                             [0.8, 99],
                             [0.7, 99],
                             [0.6, 49],
                             [0.5, 49],
                             [0.4, 49],
                             [0.3, 49],
                             [0.2, 49],
                             [0.1, 49]]

    for window_size_step in list_window_size_step:
        labels_list_detection_sliding_windows = recursive_sliding_windows(img_resized,
                                                                          int(window_size_step[0] * img_resized.shape[
                                                                              0]),
                                                                          window_size_step[1],
                                                                          spetialized_classifier=spetialized_classifier,
                                                                          display_fig=display_fig)

        labels_list_detection += labels_list_detection_sliding_windows

    labels_list_detection_non_maxima = delete_non_maxima_image(labels_list_detection)
    final_labels_list = []
    fig, ax = plt.subplots()
    ax.imshow(img)
    ax.axis('off')
    for label in labels_list_detection_non_maxima:
        if spetialized_classifier:
            window = img_resized[label[1]:label[3], label[0]:label[2]]
            # reshape de l'image
            window_reshape = transform.resize(
                window, (48, 48),
                anti_aliasing=True, mode='reflect')

            # Convertir les valeurs des pixels à l'échelle [0, 255]
            window = img_as_ubyte(window_reshape)
            window = np.clip(window, 0, 255)

            # ajout du panneau en flatten et du label
            window_flatten = window.flatten().reshape(1, -1)

            labels_name_sign = clf_sign.classes_
            labels_name_light = clf_light.classes_
            if label[4] == "sign":
                result_clf_sign = clf_sign.predict_proba(window_flatten)[0]
                label[4] = labels_name_sign[np.argmax(result_clf_sign)]
                label[5] = result_clf_sign.max()
            elif label[4] == "light":
                result_clf_light = clf_light.predict_proba(window_flatten)[0]
                label[4] = labels_name_light[np.argmax(result_clf_light)]
                label[5] = result_clf_light.max()
            if label[5] <= 0.9:
                continue
        final_labels_list.append(label)
        # Affichage des rectangles et labels sur l'image redimensionnée
        rect = plt.Rectangle((label[0], label[1]), label[2] - label[0], label[3] - label[1], edgecolor='red',
                             facecolor='none')
        ax.add_patch(rect)

        ax.text(label[0], label[1], "{} {:.2f}".format(label[4], label[5]), fontsize=12, color='red')
    if img_to_save_filename is not None:
        plt.savefig(img_to_save_filename, dpi=400)

    plt.close(fig)
    if label_filename is not None:
        save_list_csv(final_labels_list, label_filename)

    return final_labels_list


from multiprocessing import Pool
from copy import deepcopy


def detect(args):
    filename, img_folder, spetialized_classifier = args
    # Construire le chemin complet du fichier
    filepath = os.path.join(img_folder, filename)

    # Vérifier si c'est un fichier (et non un dossier)
    if os.path.isfile(filepath):
        filename_img = os.path.join(img_folder, filename)

        img_number = filename.split('/')[-1].split('.')[0]  # Extract image number from filename

        file_label_to_save = 'detection_label/' + img_number + '.csv'
        file_img_to_save = 'detection_img/' + img_number + '.jpg'
        print(f'Processing file: {filename_img}', file_label_to_save, file_img_to_save)
        detection_image(filename_img, file_label_to_save, file_img_to_save,
                        spetialized_classifier=spetialized_classifier, display_fig=False)


def detect_all_img(val_img_folder, spetialized_classifier=False, multiproc=True):
    filenames = os.listdir(val_img_folder)

    if multiproc:
        pool_work = [(filename, val_img_folder, spetialized_classifier) for filename in filenames]
        p = Pool(5)
        res = p.map(detect, pool_work)
        print("Done")
    else:
        for i, label_file in enumerate(filenames):
            detect((filenames[i], val_img_folder, spetialized_classifier))


# image test
img_test_number = '0004'
img_test_filename = '../dataset/val/images/' + img_test_number + '.jpg'
filename_label_to_save = 'detection_label/' + img_test_number + '.csv'
filename_img_to_save = 'detection_img/' + img_test_number + '.jpg'

# chargement du clf
clf = joblib.load(f'classifiers/svm_clf.pkl')
clf_type = joblib.load(f'classifiers/spe/svm_clf_type.pkl')
clf_sign = joblib.load(f'classifiers/spe/svm_clf_sign.pkl')
clf_light = joblib.load(f'classifiers/spe/svm_clf_light.pkl')

'''start = time.time()
detection_image(img_test_filename, filename_label_to_save, filename_img_to_save, clf, display_fig = False)
end = time.time()
print('time : ', end-start)'''

if __name__ == '__main__':
    val_img_folder = '../dataset/val/images'
    detect_all_img(val_img_folder, spetialized_classifier= True)

# %%
