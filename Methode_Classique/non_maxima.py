#%%
from shapely.geometry import Polygon


def recursive_delete_non_maxima_image(list_detection_img, i, debug=False):

    if debug:
        print()
        print(i)
    for row in list_detection_img:
        print(row)

    if i == len(list_detection_img):
        return list_detection_img

    # extraction de l'image de reference correspond à i
    img_ref = list_detection_img[i]
    label_ref = img_ref[4]
    x_ref, y_ref =  int(img_ref[0]), int(img_ref[1])
    window_size_x_ref, window_size_y_ref = int(img_ref[2]), int(img_ref[3])

    # initialisation de la liste qui va etre renvoyé au prochain appel
    new_list_detection_img = []
    for j in range(i+1):
        new_list_detection_img.append(list_detection_img[j])

    polygon_ref = Polygon([(x_ref, y_ref), (x_ref+window_size_x_ref, y_ref), (x_ref+window_size_x_ref, y_ref+window_size_y_ref), (x_ref, y_ref+window_size_y_ref)])

    # parcours de toutes images à partir de l'image de ref
    for j in range(i+1, len(list_detection_img)):

        x_j, y_j = int(list_detection_img[j][0]), int(list_detection_img[j][1])    
        window_size_x_j, window_size_y_j = int(list_detection_img[j][2]), int(list_detection_img[j][3])

        polygon_j = Polygon([(x_j, y_j), (x_j+window_size_x_j, y_j), (x_j+window_size_x_j, y_j+window_size_y_j), (x_j, y_j+window_size_y_j)])
        
        intersection = polygon_ref.intersection(polygon_j)
        union = polygon_ref.union(polygon_j)
        recouvrement = intersection.area / union.area

        intersection2 = polygon_j.intersection(polygon_ref)
        union2 = polygon_j.union(polygon_ref)
        recouvrement2 = intersection2.area / union2.area

        if debug:
            print(recouvrement, intersection.area, polygon_j.area, label_ref, list_detection_img[j][4])


        if label_ref == list_detection_img[j][4]:
            if recouvrement <= 0:
                new_list_detection_img.append(list_detection_img[j])
        else:  
            #if recouvrement < 0.5 and intersection.area != polygon_j.area:
            if recouvrement < 0.5 and intersection.area <= 0.5*polygon_j.area:
            #if recouvrement < 0.2:
                new_list_detection_img.append(list_detection_img[j])

    if debug:
        for row in new_list_detection_img:
            print(row)
    # condition d'arret
    return recursive_delete_non_maxima_image(new_list_detection_img, i+1)
    

def correct_coordinate(img_detection_non_maxima):
    for row in img_detection_non_maxima:
        x, y, length, height = map(int, row[0:4])

        # Mise à jour des coordonnées dans row
        row[0] = x
        row[1] = y
        row[2] = x + length
        row[3] = y + height
    return img_detection_non_maxima

def delete_non_maxima_image(list_label_detection):

    # ouverture du fichier 
    # Liste pour stocker les données

    # trie des detections par score decroissant
    list_label_detection.sort(key=lambda x: float(x[5]), reverse=True)
    
    img_detection_non_maxima = recursive_delete_non_maxima_image(list_label_detection, 0)
    img_detection_non_maxima_corrected = correct_coordinate(img_detection_non_maxima)

    return img_detection_non_maxima_corrected

